// Small helper to make up for the lack of a built-in fold function on arrays
export function foldl<T,U>(
  array: readonly T[], 
  callbackfn: (previousValue: U, currentValue: T) => U, 
  initialValue: U
): U {
  var current = initialValue
  array.forEach(e => current = callbackfn(current, e))
  return current
}

