import {Article, Database, DiscountType} from "./model";
import {
  applyDiscount, computeChargedDeliveryFee, computeChargedPrice, computeItemsTotal
} from "./processing";

test('computeItemsTotal must return correct total', () => {
  expect(
    computeItemsTotal(
      new Map<number, Article>([
        [1, { id: 1, name: 'coffee', price: 200 }],
        [2, { id: 2, name: 'tea', price: 450 }],
        [3, { id: 3, name: 'milk', price: 600 }]
      ]),
      [
        { article_id: 3, quantity: 5 },
        { article_id: 1, quantity: 3 }
      ] 
    )
  ).toBe(3600);
});


test('computeChargedDeliveryFee must return correct fee', () => {
  const fees = [
    {
      "eligible_transaction_volume": {
        "min_price": 0,
        "max_price": 500
      },
      "price": 30
    },
    {
      "eligible_transaction_volume": {
        "min_price": 500,
        "max_price": 1500
      },
      "price": 45
    },
    {
      "eligible_transaction_volume": {
        "min_price": 1500,
        "max_price": null
      },
      "price": 60
    }
  ]
  expect(() => computeChargedDeliveryFee(fees, -1)).toThrow();
  expect(computeChargedDeliveryFee(fees, 0)).toBe(30);
  expect(computeChargedDeliveryFee(fees, 320)).toBe(30);
  expect(computeChargedDeliveryFee(fees, 500)).toBe(45);
  expect(computeChargedDeliveryFee(fees, 900)).toBe(45);
  expect(computeChargedDeliveryFee(fees, 1500)).toBe(60);
  expect(computeChargedDeliveryFee(fees, 3000)).toBe(60);
});


test('applyDiscount must subtract the correct amount', () => {
  expect(
    applyDiscount(
      1000, 
      {
        "article_id": 123,
        "type": DiscountType.Percentage,
        "value": 25
      }
    )
  ).toBe(750)
  expect(
    applyDiscount(
      1000, 
      {
        "article_id": 123,
        "type": DiscountType.Amount,
        "value": 150
      }
    )
  ).toBe(850)
});


test('computeChargedPrice must return correct charged price', () => {
  const db: Database = {
    articles: new Map<number, Article>([
      [1, { id: 1, name: 'coffee', price: 200 }],
      [2, { id: 2, name: 'tea', price: 450 }],
      [3, { id: 3, name: 'milk', price: 600 }],
      [4, { id: 4, name: 'water', price: 100 }],
      [5, { id: 5, name: 'lemonade', price: 750 }]
    ]),
    delivery_fees: [
      {
        "eligible_transaction_volume": {
          "min_price": 0,
          "max_price": 1000
        },
        "price": 30
      },
      {
        "eligible_transaction_volume": {
          "min_price": 1000,
          "max_price": null
        },
        "price": 20
      }
    ],
    discounts: [
      {
        "article_id": 4,
        "type": DiscountType.Percentage,
        "value": 25
      },
      {
        "article_id": 5,
        "type": DiscountType.Amount,
        "value": 130
      }
    ]
  };

  expect(
    computeChargedPrice(
      db, 
      [
        { article_id: 1, quantity: 2 },
        { article_id: 2, quantity: 1 }
      ]
    )
  ).toBe(880);

  expect(
    computeChargedPrice(
      db, 
      [
        { article_id: 3, quantity: 5 },
        { article_id: 1, quantity: 3 }
      ]
    )
  ).toBe(3620);
   

  expect(
    computeChargedPrice(
      db, 
      [
        { article_id: 4, quantity: 2 },
        { article_id: 5, quantity: 3 }
      ]
    )
  ).toBe(2030);
});