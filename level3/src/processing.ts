import {
  Article, Cart, Database, DeliveryFee, Discount, DiscountType, Item, ProcessedCart
} from "./model";
import {foldl} from "./utils"

// Processes a cart, computing the total charged price for the input cart
export function processCart(database: Database, cart: Cart): ProcessedCart {
  return {
    id: cart.id,
    total: computeChargedPrice(database, cart.items)
  };
};

export function processCarts(database: Database, carts: readonly Cart[]): readonly ProcessedCart[] {
  return carts.map(cart => processCart(database, cart));
};

// Applies a discount to a given price
export function applyDiscount(price: number, discount: Discount): number {
  switch(discount.type) {
    case DiscountType.Amount:
      return price - discount.value
    case DiscountType.Percentage:
      // NOTE: the result is truncated ("rounded down")
      return Math.floor((price * (100 - discount.value)) / 100);
  }
}

// Computes the total price for a given cart item, including discounts (if any)
export function computeArticlePrice(
  articles: ReadonlyMap<number, Article>, 
  articleId: number,
  discounts: readonly Discount[] = []
): number {
  const article = articles.get(articleId)
  if (!article) {
    throw new Error(`Article ${articleId} not found`)
  }
  const applicableDiscounts = discounts.filter(discount => discount.article_id == articleId)
  // Apply all relevant discounts, starting from the article price
  // Note that the result is order dependent. We simply apply the discount
  // in the same order as they are provided.
  return foldl(
    applicableDiscounts, 
    (currentPrice, discount) => applyDiscount(currentPrice, discount),
    article.price
  );
}

// Computes the total price for a given cart item, including discounts (if any)
export function computeItemPrice(
  articles: ReadonlyMap<number, Article>, 
  item: Item,
  discounts: readonly Discount[] = []
): number {
  return item.quantity * computeArticlePrice(articles, item.article_id, discounts);
};

// Computes the total price for a given list of cart items, including discounts (if any)
export function computeItemsTotal(
  articles: ReadonlyMap<number, Article>, 
  items: readonly Item[],
  discounts: readonly Discount[] = []
): number {
  return items
    .map(item => computeItemPrice(articles, item, discounts))
    .reduce((l, r) => l + r, 0);
};

// Computes the delivery fee that is charged, depending on the cart's total price
export function computeChargedDeliveryFee(delivery_fees: readonly DeliveryFee[], total: number): number {
  const fee = delivery_fees.find(fee => 
    (total >= fee.eligible_transaction_volume.min_price) &&
    (!fee.eligible_transaction_volume.max_price || (total < fee.eligible_transaction_volume.max_price))
  )
  if (!fee) {
    throw new Error(`Could not compute delivery fee for total ${total}`);
  }
  return fee.price;
}

// Computes the price that is charged for a given list of items, 
// including discounts (if any) and shipping fees
export function computeChargedPrice(database: Database, items: readonly Item[]): number {
  const total = computeItemsTotal(database.articles, items, database.discounts);
  const deliveryFee = computeChargedDeliveryFee(database.delivery_fees, total);
  return total + deliveryFee;
}
