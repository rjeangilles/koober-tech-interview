import fs from "fs";
import {Article, Cart, Database, DeliveryFee, Discount, ProcessedCart} from "./model";

interface InputFileData {
  readonly articles: readonly Article[];
  readonly carts: readonly Cart[];
  readonly delivery_fees: readonly DeliveryFee[];
  readonly discounts: readonly Discount[];
};

// Reads the input data file.
export function readInputData(path: string): readonly [Database, readonly Cart[]] {
  // WARN: unchecked cast
  const fileData = JSON.parse(fs.readFileSync(path, 'utf8')) as InputFileData;
  const articlesById: ReadonlyMap<number, Article> = new Map(fileData.articles.map(a => [a.id, a]));
  const database: Database = {
    articles: articlesById,
    delivery_fees: fileData.delivery_fees,
    discounts: fileData.discounts
  };
  const carts = fileData.carts;
  return [database, carts];
};

export function writeOutputData(path: string, processedCarts: readonly ProcessedCart[]): void {
  const outputData = {
    carts: processedCarts
  };
  fs.writeFileSync(path, JSON.stringify(outputData, null, 2), 'utf8');
}
