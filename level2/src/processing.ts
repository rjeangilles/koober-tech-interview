import {Article, Cart, Database, DeliveryFee, Item, ProcessedCart} from "./model";

// Processes a cart, computing the total charged price for the input cart
export function processCart(database: Database, cart: Cart): ProcessedCart {
  return {
    id: cart.id,
    total: computeChargedPrice(database, cart.items)
  };
};

export function processCarts(database: Database, carts: readonly Cart[]): readonly ProcessedCart[] {
  return carts.map(cart => processCart(database, cart));
};

// Computes the total price for a given list of cart items
export function computeItemsTotal(articles: ReadonlyMap<number, Article>, items: readonly Item[]): number {
  return items
    .map(item => {
      const article = articles.get(item.article_id)
      if (!article) {
        throw new Error(`Article ${item.article_id} not found`)
      }
      return item.quantity * article.price
    })
    .reduce((l, r) => l + r, 0);
};

// Computes the delivery fee that is charged, depending on the cart's total price
export function computeChargedDeliveryFee(delivery_fees: readonly DeliveryFee[], total: number): number {
  const fee = delivery_fees.find(fee => 
    (total >= fee.eligible_transaction_volume.min_price) &&
    (!fee.eligible_transaction_volume.max_price || (total < fee.eligible_transaction_volume.max_price))
  )
  if (!fee) {
    throw new Error(`Could not compute delivery fee for total ${total}`);
  }
  return fee.price;
}

// Computes the price that is cahrged for a given list of items, including shipping fees
export function computeChargedPrice(database: Database, items: readonly Item[]): number {
  const total = computeItemsTotal(database.articles, items);
  const deliveryFee = computeChargedDeliveryFee(database.delivery_fees, total);
  return total + deliveryFee;
}
