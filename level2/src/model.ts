export interface Article {
  readonly id: number;
  readonly name: string;
  readonly price: number;
};

export interface Cart {
  readonly id: number;
  readonly items: readonly Item[];
};

export interface Item {
  readonly article_id: number;
  readonly quantity: number;
};

export interface DeliveryFee {
  readonly eligible_transaction_volume: {
    readonly min_price: number;
    readonly max_price: number | null;
  };
  readonly price: number
};

export interface ProcessedCart {
  readonly id: number;
  readonly total: number;
};

export interface Database {
  readonly articles: ReadonlyMap<number, Article>;
  readonly delivery_fees: readonly DeliveryFee[];
}
