import fs from "fs";
import {Article, Cart, ProcessedCart} from "./model";
import {readInputData, writeOutputData} from "./io"
import {processCarts} from "./processing"

function main() {
  // Read the input data from the file
  const [database, carts] = readInputData('input.json');

  // Process the carts
  const processedCarts = processCarts(database, carts);

  // Write the result to disk
  writeOutputData('output.json', processedCarts);
}

if (require.main === module) {
  main();
}