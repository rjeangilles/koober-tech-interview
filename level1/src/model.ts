export interface Article {
  readonly id: number;
  readonly name: string;
  readonly price: number;
};

export interface Cart {
  readonly id: number;
  readonly items: readonly Item[];
};

export interface Item {
  readonly article_id: number;
  readonly quantity: number;
};

export interface ProcessedCart {
  readonly id: number;
  readonly total: number;
};

export interface Database {
  readonly articles: ReadonlyMap<number, Article>;
}
