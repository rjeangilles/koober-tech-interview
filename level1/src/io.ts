import fs from "fs";
import {Database, Article, Cart, ProcessedCart} from "./model";

interface InputFileData {
  readonly articles: readonly Article[];
  readonly carts: readonly Cart[];
};

// Reads the input data file.
export function readInputData(path: string): readonly [Database, readonly Cart[]] {
  // WARN: unchecked cast
  const fileData = JSON.parse(fs.readFileSync(path, 'utf8')) as InputFileData;
  const database: Database = {
    articles: new Map<number, Article>(fileData.articles.map(a => [a.id, a])) 
  };
  const carts = fileData.carts;
  return [database, carts];
};

export function writeOutputData(path: string, processedCarts: readonly ProcessedCart[]): void {
  const outputData = {
    carts: processedCarts
  };
  fs.writeFileSync(path, JSON.stringify(outputData, null, 2), 'utf8');
}
