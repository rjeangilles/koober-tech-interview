import {Article, Database} from "./model";
import {processCart} from "./processing";

const db: Database = {
  articles: new Map<number, Article>([
    [1, { id: 1, name: 'coffee', price: 200 }],
    [2, { id: 2, name: 'tea', price: 450 }],
    [3, { id: 3, name: 'milk', price: 600 }]
  ])
}

test('processCart must return cart with same id and correct total', () => {
  expect(
    processCart(
      db, 
      { 
        id: 123, 
        items: [
          { article_id: 3, quantity: 5 },
          { article_id: 1, quantity: 3 }
        ] 
      }
    )
  ).toStrictEqual({
    id: 123,
    total: 3600
  });
});