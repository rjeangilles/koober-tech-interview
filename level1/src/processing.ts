import {Cart, Database, Item, ProcessedCart} from "./model";

// Processes a cart, computing the total price of the input cart
export function processCart(database: Database, cart: Cart): ProcessedCart {
  return {
    id: cart.id,
    total: computeItemsTotal(database, cart.items)
  };
};

export function processCarts(database: Database, carts: readonly Cart[]): readonly ProcessedCart[] {
  return carts.map(cart => processCart(database, cart));
};

// Computes the total price for a given list of cart items
function computeItemsTotal(database: Database, items: readonly Item[]): number {
  return items
    .map(item => {
      const article = database.articles.get(item.article_id)
      if (!article) {
        throw new Error(`Article ${item.article_id} not found`)
      }
      return item.quantity * article.price
    })
    .reduce((l, r) => l + r, 0);
};
